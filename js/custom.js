// Custom JS 

/*
* @author: Faisal Saeed
*/

/*app carousel*/
$(document).ready(function () {

  var swiper = new Swiper(".property-slider", {
    slidesPerView: 1,
    spaceBetween: 0,
    grabCursor: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 0,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 0,
      },
      1024: {
        slidesPerView: 2,
        spaceBetween: 0,
        grid: {
          rows: 2,
        },
      },
      1645: {
        slidesPerView: 3,
        spaceBetween: 0,
        grid: {
          rows: 2,
        },
      },
    },
  });

  /*agents*/
  var swiper = new Swiper(".agents-slider", {
    slidesPerView: 1,
    spaceBetween: 0,
    grabCursor: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      640: {
        slidesPerView: 1,
        spaceBetween: 0,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 0,
      },
      1024: {
        slidesPerView: 2,
        spaceBetween: 0,
        grid: {
          rows: 2,
        },
      },
      1645: {
        slidesPerView: 2,
        spaceBetween: 0,
        grid: {
          rows: 2,
        },
      },
    },
  });


  var swiper = new Swiper(".property-thumbs-holders", {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
  });
  var swiper2 = new Swiper(".single-property-thumbs-slider", {
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    thumbs: {
      swiper: swiper,
    },
  });


  /*add class in header*/
  $(window).on("scroll", function () {
    if ($(window).scrollTop() >= 50) {
      $("header").addClass("header-scroll ");
    } else {
      $("header").removeClass("header-scroll ");
    }
  });



  /*scroll to top*/
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });
  $('.scrollup').click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 600);
    return false;
  });
});



